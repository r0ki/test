// highlight test
#include <cstddef>
#include <cstdint>
#include <typeinfo>
#include <new>

#define CPP __cplusplus
#define CPP11 201103L

#if CPP >= 201703L // [cpp.predefined]
#    include <iterator>
template <class T>
concept C = requires(T x)
{
    {
        x.begin()
    }
    ->typename T::iterator;
}; // NOT Concept TS
#endif

// Module TS
// import std.core;
/*export*/ class MT {
};
// module

extern int l;

namespace ns {
}

int main()
{
    using namespace ns;
    union uni {
    };
    typedef int typedeftest;
    typeid(typedeftest);
    static long int n;
    bool();
    char();
    float();
    short();
    signed();
    unsigned();
    wchar_t();
    const long int* np = &n;
    ::new (&n) int;
    sizeof(1);
    const_cast<long int*>(np);
    static_cast<double>(n);
    while (true) {
        break;
        continue;
    }
    for (;;) { break; }
    do {
    } while (false);
    switch (n) {
    case 42:
    default: break;
    }
    try {
        throw 42;
    } catch (int) {
    }
    compl 1;
    if (true)
        ;
    else
        ;
    enum {};
    goto LA;
LA:;
    not 1;
    n not_eq 1;
    1 or 1;
    n or_eq 1;
    1 xor 1;
    n xor_eq 1;
    void* p = &n;
    reinterpret_cast<long int*>(p);
#if CPP >= CPP11
    struct alignas(8) S{};
    alignof(std::max_align_t);
    [[maybe_unused]] auto a = 42;
    char16_t();
    char32_t();
    [[maybe_unused]] constexpr std::size_t i = 42;
    decltype(a)();
    class X {
        friend struct S;
    private:
    protected:
    public:
        inline virtual void f() { return; };
        explicit X() = default;
        X(const X&) = delete;
    } x;
    const volatile class Y final : public X {
        mutable int x;
        Y& operator=(const Y&) = delete;
        void f() override {}
    }* y = dynamic_cast<Y*>(&x);
    static_assert(noexcept(1));
    nullptr;
#else
    auto int a = 42;
    register int x = 42;
#endif
    1 and 1;
    n and_eq 1;
    1 bitand 1;
    asm("movq $60, %rax\n\t" // the exit syscall number on Linux
        "movq $0,  %rdi\n\t"
        "syscall");
}
